import{ Schema } from 'mongoose';
import crypto from 'crypto';
import mongoose from 'mongoose';

export interface Message extends Document {
  username: string
  text: string
  createdAt: Date
  channel: string
}
const MessageSchema: Schema<Message> = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  channel: {
    type:String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});



// export model user with UserSchema
export default mongoose.model("message", MessageSchema);
