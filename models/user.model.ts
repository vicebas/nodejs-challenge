import{ Schema } from 'mongoose';
import crypto from 'crypto';
import mongoose from 'mongoose';

export interface User  extends Document {
  username: string
  hash: string
  salt: string
  createdAt: Date
}
const UserSchema: Schema<User> = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  hash: String,
  salt: String,
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});


UserSchema.methods.setPassword = function(password: string) {
     
    this.salt = crypto.randomBytes(16).toString('hex');
  
    this.hash = crypto.pbkdf2Sync(password, this.salt, 
    1000, 64, `sha512`).toString(`hex`);
};
  

UserSchema.methods.validPassword = function(password) {
    const hash = crypto.pbkdf2Sync(password, 
    this.salt, 1000, 64, `sha512`).toString(`hex`);
    return this.hash === hash;
};
  

// export model user with UserSchema
export default mongoose.model('user', UserSchema);
