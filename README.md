Simple chat with bot integration

## Developing

### Configuring node dependencies

Use the following procedure to setup the dev environment:

- Run `npm install`
- Copy the .env.development file to .env
### Running dependencies

This simple chat depends on 3 different services Running:
 - RabbitMQ
 - MongoDB
 - [NodeJS Challenge Bot](https://gitlab.com/vicebas/nodejs-challenge-bot)
 
 You can run every dependencies withe the docker-compose file inside the project.
 Just run the following commands:

```bash
docker compose up
```


### Running locally

Run the following commands:

```bash
npm start
```

After running, frontend will be available on [http://localhost:3000](http://localhost:3000) 

## Languages & tools

### Node.js base dependencies

- [Typescript](https://www.typescriptlang.org/) Superset of javascript that compiles to plain javascript
- [RabbitMQ](https://www.rabbitmq.com/) RabbitMQ is a message broker:
- [mongoose](https://www.mongoosejs.com/) Mongoose provides a straight-forward, schema-based solution to model your application data.
- [Socket.IO ](https://socket.io/) Socket.IO enables real-time, bidirectional and event-based communication
- [Express.js](https://expressjs.com/pt-br/) Fast, unopinionated, minimalist web framework for Node.js


