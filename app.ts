import express from "express";
import * as path from "path" ;
import  logger  from "morgan";
import SessionService from "./services/sessions.service";
import DbService from "./services/db.service";
import QueueService from "./services/queue.service";
 import { Express} from 'express-serve-static-core';
import ApiService from "./services/api.service";
import messageModel from "./models/message.model";


var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var chatsRouter = require("./routes/chats");

class App {
express: Express;
  constructor() {
    this.express = express();

    this.database();
    this.middlewares();
    this.routes();


  }

  database() {
    DbService.startDatabase()
  }

  middlewares() {
    this.express.use(logger("dev"));
    this.express.use(express.json());
    this.express.use(express.urlencoded({ extended: false }));
    this.express.set("view engine", "ejs");
    QueueService.startService((msg:string) =>{
        const data = JSON.parse(msg)
        ApiService.receiveMessage(new messageModel(data))
    });
    SessionService.decorateApp(this.express);
    this.express.use(express.static(path.join(__dirname, "public")));
  }

  routes() {
    this.express.use("/", indexRouter);
    this.express.use("/users", usersRouter);
    this.express.use("/chats", chatsRouter);


  }



}

export default (new App().express);