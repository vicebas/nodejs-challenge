"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path = __importStar(require("path"));
const morgan_1 = __importDefault(require("morgan"));
const sessions_service_1 = __importDefault(require("./services/sessions.service"));
const db_service_1 = __importDefault(require("./services/db.service"));
const queue_service_1 = __importDefault(require("./services/queue.service"));
const api_service_1 = __importDefault(require("./services/api.service"));
const message_model_1 = __importDefault(require("./models/message.model"));
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var chatsRouter = require("./routes/chats");
class App {
    constructor() {
        this.express = express_1.default();
        this.database();
        this.middlewares();
        this.routes();
    }
    database() {
        db_service_1.default.startDatabase();
    }
    middlewares() {
        this.express.use(morgan_1.default("dev"));
        this.express.use(express_1.default.json());
        this.express.use(express_1.default.urlencoded({ extended: false }));
        this.express.set("view engine", "ejs");
        queue_service_1.default.startService((msg) => {
            const data = JSON.parse(msg);
            api_service_1.default.receiveMessage(new message_model_1.default(data));
        });
        sessions_service_1.default.decorateApp(this.express);
        this.express.use(express_1.default.static(path.join(__dirname, "public")));
    }
    routes() {
        this.express.use("/", indexRouter);
        this.express.use("/users", usersRouter);
        this.express.use("/chats", chatsRouter);
    }
}
exports.default = (new App().express);
