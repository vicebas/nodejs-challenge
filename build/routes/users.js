"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const user_model_1 = __importDefault(require("../models/user.model"));
const sessions_service_1 = __importDefault(require("../services/sessions.service"));
var router = express_1.default.Router();
router.post("/autenticate", function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        let username = req.body.username;
        let password = req.body.password;
        if (yield sessions_service_1.default.authenticate(req, res, username, password)) {
            return res.redirect("/chats");
        }
        return res.redirect("/?login=error");
    });
});
router.post("/signup", function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        const username = req.body.username;
        const password = req.body.password;
        const user = new user_model_1.default({
            username
        });
        user.setPassword(password);
        console.log(user);
        yield user.save();
        if (yield sessions_service_1.default.authenticate(req, res, username, password)) {
            return res.redirect("/chats");
        }
        return res.redirect("/");
    });
});
module.exports = router;
