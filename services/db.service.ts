
import mongoose, {Mongoose} from 'mongoose';
import dotenv from "dotenv";

dotenv.config();

// Replace this with your MONGOURI.
const DB_USER  = process.env.DB_USER;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_URL = process.env.DB_URL;
const MONGOURI = `mongodb://${DB_USER}:${DB_PASSWORD}@${DB_URL}`;

export default class DbService {
  private static db: Mongoose;
  

    public static startDatabase = async  (): Promise<Mongoose> => {
        if (!DbService.db){
            try {
              DbService.db = await mongoose.connect(MONGOURI, {
                useNewUrlParser: true,
              });
            } catch (e) {
              console.log(e);
              throw e;
            }
        }
        return DbService.db;
    }
}
