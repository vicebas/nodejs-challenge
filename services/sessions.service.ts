import cookieParser from "cookie-parser";
import httpContext from "express-http-context";
import {
  Express,
  RequestHandler,
  Request,
  Response,
} from "express-serve-static-core";
import crypto from "crypto";
import dotenv from "dotenv";
import UserModel, { User } from "../models/user.model";

dotenv.config();

const SECRET = process.env.SECRET as string;
const KEY = process.env.KEY as string;

export default class SessionService {
  private static authTokens: Record<string, any> = {};
  public static cookie: RequestHandler;
  public static async authenticate(
    req: Request,
    res: Response,
    username: string,
    password: string
  ) {
    const user = await UserModel.findOne({ username });
    if (!user || !user.validPassword(password)) {
      console.log("fake password");
      return false;
    }
    const authToken = this.generateAuthToken();

    // Store authentication token
    SessionService.authTokens[authToken] = user;

    // Setting the auth token in cookies
    res.cookie("AuthToken", authToken);
    httpContext.set("user", SessionService.authTokens[authToken]);

    return user;
  }
  private static generateAuthToken = () => {
    return crypto.randomBytes(30).toString("hex");
  };
  private static getUserByToken(token: string) {
    return SessionService.authTokens[token];
  }
  public static getUsernameByToken(token: string) {
    if (!SessionService.getUserByToken(token)){
      return false
    }
      return SessionService.getUserByToken(token)["username"];
  }

  private static getUser() {
    return httpContext.get("user");
  }
  public static getUsername() {
    return SessionService.getUser().username;
  }

  public static isAuthenticated(req: Request) {
    return SessionService.getUser() && SessionService.getUser().username;
  }

  public static decorateApp(app: Express) {
    SessionService.cookie = cookieParser();
    app.use(SessionService.cookie);
    app.use(httpContext.middleware);
    app.use((req, res, next) => {
      // Get auth token from the cookies
      const authToken = req.cookies["AuthToken"];

      // Inject the user to the request

      httpContext.set("user", SessionService.authTokens[authToken]);

      next();
    });
  }
}
