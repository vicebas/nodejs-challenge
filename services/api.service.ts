import { Message } from "../models/message.model";
import dotenv from "dotenv";
import axios from "axios";
import SocketService from "./socket.service";
dotenv.config()


const WEBHOOK_URL = process.env.WEBHOOK;
const BOT_KEY = process.env.BOT_KEY;
export default class ApiService {
  static async sendMessage(message: Message) {
    if(WEBHOOK_URL){
      return (await axios.post(WEBHOOK_URL, JSON.stringify(message))).data;
    }
  }
  static receiveMessage( message: Message) {
    console.log(message)
      SocketService.serverMessage(message)
  }

  static AuthBot(bot_key: string) {
    
    return bot_key.replace("Basic ","") == BOT_KEY;
  }
}