import amqp from "amqplib/callback_api";
import dotenv from "dotenv";

dotenv.config();
const QUEUE = process.env.QUEUE || "botQUEUE";

export default class MqService {
  public static startService( callback:Function) {
    amqp.connect("amqp://localhost", function (error0, connection) {
      if (error0) {
        throw error0;
      }
      connection.createChannel(function (error1, channel) {
        if (error1) {
          throw error1;
        }

        channel.assertQueue(QUEUE, {
          durable: false,
        });
        console.log(
          " [*] Waiting for messages in %s. To exit press CTRL+C",
          QUEUE
        );
        channel.consume(
          QUEUE,
          function (msg: amqp.Message | null) {
            if (msg) {
              console.log(" [x] Received %s", msg.content.toString());

              callback( msg.content.toString());
            }
          },
          {
            noAck: true,
          }
        );
      });
    });
  }

  public static sendMessage = (msg: string) => {
    amqp.connect("amqp://localhost", function (error0, connection) {
      if (error0) {
        throw error0;
      }
      connection.createChannel(function (error1, channel) {
        if (error1) {
          throw error1;
        }

        channel.assertQueue(QUEUE, {
          durable: false,
        });

        channel.sendToQueue(QUEUE, Buffer.from(msg));
        console.log(" [x] Sent %s", JSON.stringify(msg));
      });
    });
  };
}
