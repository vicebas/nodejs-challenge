import { Server } from "http";
import { Server as SocketServer, Socket } from "socket.io";
import { Express, Request, Response } from "express-serve-static-core";
import SessionService from "./sessions.service";
import moment from "moment";
import dotenv from "dotenv";
import { Session } from "inspector";
import MessageModel, { Message } from "../models/message.model";
import { NextFunction } from "express";
import ApiService from "./api.service";
import userModel, { User } from "../models/user.model";
dotenv.config();

interface ISocket extends Socket {
  username?: string;
  // other additional attributes here, example:
  // surname?: string;
}
const KEY = process.env.KEY as string;

export default class SocketService {
  private static socketIo: SocketServer;

  public static getUsernameByCookie = (cookieString: string) => {
    if (!cookieString) {
      return false;
    }

    const cookie = cookieString
      .split(";")
      .filter((c) => c.trim().split("=").includes("AuthToken"))
      .pop();

    if (!cookie) {
      return false;
    }
    console.log(cookie.replace("AuthToken=", ""));
    return SessionService.getUsernameByToken(
      cookie.replace("AuthToken=", "").trim()
    );
  };
  public static startSocket = (httpServer: Server, app: Express): void => {
    SocketService.socketIo = new SocketServer(httpServer, {
      cors: {
        origin: ["http://localhost", "null"],
        methods: ["GET", "POST"],
      },
    });
    SocketService.socketIo.use(async (socket: ISocket, next) => {
      if (!socket.request.headers.cookie) {
        return next(new Error("invalid username"));
      }

      const username = SocketService.getUsernameByCookie(
        socket.request.headers.cookie
      );

      if (!username) {
        return next(new Error("invalid username"));
      }
      socket.username = username;
      next();
    });

    SocketService.socketIo.on("connection",  async (socket: ISocket) => {
      let users = await userModel.find().limit(10).exec()
      users = users.map( (user: User) => ({username: user.username, status: false}))
      console.log(users)
      for (let [id, strangerSocket] of SocketService.socketIo.of("/").sockets) {
        users[
          users.findIndex(
            (user: any) => user.username == (strangerSocket as ISocket).username
          )
        ].status = true;
      }
      socket.emit("users", users);
      socket.broadcast.emit("users", users);
      socket.emit("user", socket.username);
      socket.on("reclaimHistory", (msg) => {
      MessageModel.find({channel: msg})
        .sort("-createdAt")
        .limit(50)
        .exec(function (err: any, messages: Message[]) {
          const history = messages.reverse();
          socket.emit("history", history);
        });
      })
      socket.on("message", ({msg, channel}) => {
        console.log(msg)
        const message = new MessageModel({
          username: socket.username,
          text: msg,
          channel
        });
        const command = SocketService.getCommand(msg);
        if (command) {
          SocketService.clientMessage(message);
          try {
            return ApiService.sendMessage(
              new MessageModel({
                username: socket.username,
                text: command,
                createdAt: new Date(),
                channel
              })
            );
          } catch (ex) {
            return SocketService.serverMessage(
              new MessageModel({
                username: "errorTracker",
                text: "The bot is offline",
                createdAt: new Date(),
                channel,
              })
            );
          }
        }

        message.save();
        return SocketService.clientMessage(message);
      });
    });
  };
  public static getCommand(msg: String) {
    if (msg.substr(0, 1) == "\\") {
      return msg.substr(1);
    }
    return false;
  }
  public static clientMessage = (message: Message) => {
    SocketService.socketIo.emit(message.channel, message);
  };
  public static serverMessage = (message: Message) => {
    message.username = `BOT:${message.username}`
    MessageModel.create(message)
    SocketService.socketIo.emit(message.channel, message);

  
  };
}
