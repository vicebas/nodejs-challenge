import express from "express";
import { Request, Response } from "express-serve-static-core";
import UserModel from "../models/user.model";
import SessionService from "../services/sessions.service";

var router = express.Router();

router.post("/autenticate", async function (req: Request, res: Response, next) {
  let username = req.body.username;
  let password = req.body.password;
  if (await SessionService.authenticate(req, res,username, password)) {
    return res.redirect("/chats");
  }
  return res.redirect("/?login=error");

});

router.post("/signup", async function (req: Request, res: Response, next) {
  const username = req.body.username;
  const password = req.body.password;
  const user = new UserModel({
    username
  });
  user.setPassword(password)
  console.log(user)
  await user.save();
  if (await SessionService.authenticate(req, res, username, password)) {
    return res.redirect("/chats");
  }
  return res.redirect("/");
});
module.exports = router;
