import express from 'express';
 import { Request, Response } from 'express-serve-static-core';

const router = express.Router();

/* GET home page. */
router.get('/',(req: Request, res: Response, next) => {
  res.render("index", { error: req.query.login == 'error'});
});

router.get("/signup", (req: Request, res: Response, next) => {
  res.render("create_user", {});
});



module.exports = router;
