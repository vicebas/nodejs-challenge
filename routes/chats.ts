import express from "express";
import { Request, Response } from "express-serve-static-core";
import SessionService from "../services/sessions.service";
import ApiService from "../services/api.service";
import MessageModel from "../models/message.model";

const router = express.Router();

/* GET home page. */

router.get("/", (req: Request, res: Response) => {
  if (!SessionService.isAuthenticated(req)) {
    res.redirect("/");
  }
  res.render("chats", {channel: 'general'});
});

router.get("/:channel", (req: Request, res: Response) => {
  if (!SessionService.isAuthenticated(req)) {
    res.redirect("/");
  }
  res.render("chats", { channel: req.params.channel.toLowerCase() });
});

router.post("/message", (req: Request, res: Response) => {
  if (
    !req.headers.authorization ||
    !ApiService.AuthBot(req.headers.authorization)
  ) {
    res.sendStatus(403);
  }
  console.log(req.body);
  let message = new MessageModel({
    username: "BOT:"+req.body.username,
    text: req.body.text,
  })
  message.save()
  ApiService.receiveMessage(message)
  res.sendStatus(200)
});
module.exports = router;
