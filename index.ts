import app from "./app";
import { createServer } from "http";
import SocketService from "./services/socket.service";

// rest of the code remains same
const PORT = 3000;

const httpServer = createServer(app)
SocketService.startSocket(httpServer, app);
httpServer.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
});
